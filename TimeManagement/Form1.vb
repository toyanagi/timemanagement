﻿Imports System.Text
Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Windows.Forms
Imports Microsoft.Win32

Public Class Form1

    'ウィンドウのキャプションタイトルを取得する(P92)
    Public Declare Function GetWindowText Lib "user32.dll" Alias "GetWindowTextA" _
        (ByVal hWnd As IntPtr, _
         ByVal lpString As System.Text.StringBuilder, _
         ByVal nMaxCount As Integer) As Integer

    'ウィンドウのプロセスIDを取得
    Declare Function GetWindowThreadProcessId Lib "user32" _
        (ByVal hWnd As IntPtr, ByRef lpdwProcessId As Integer) As Integer

    'OpenProcess
    Declare Function OpenProcess Lib "kernel32" _
        (ByVal dwDesiredAccess As Integer, ByVal bInheritHandle As Integer, ByVal dwProcessId As Integer) As Integer
    Const PROCESS_VM_READ As Long = &H10
    Const PROCESS_QUERY_INFORMATION As Long = &H400

    ' EnumProcessModules
    Declare Function EnumProcessModules Lib "psapi" _
         (ByVal hProcess As Integer, _
          ByRef hMod As Integer, _
          ByVal sizehMod As Integer, _
          ByRef dwlpdwPIDsize As Integer) As Integer

    'GetModuleFileNameEx
    Declare Function GetModuleFileNameEx Lib "psapi" _
        Alias "GetModuleFileNameExA" _
        (ByVal hProcess As Integer, _
        ByVal hMod As Integer, _
        ByVal szFileName As System.Text.StringBuilder, _
        ByVal nSize As Integer) As Integer

    'GetModuleBaseName
    Declare Function GetModuleBaseName Lib "psapi" _
        Alias "GetModuleBaseNameA" _
        (ByVal hProcess As Integer, _
        ByVal hMod As Integer, _
        ByVal szFileName As System.Text.StringBuilder, _
        ByVal nSize As Integer) As Integer

    'QueryFullProcessImageName
    Declare Function QueryFullProcessImageName Lib "kernel32" Alias "QueryFullProcessImageNameA" _
        (hprocess As IntPtr, dwFlags As Integer, lpExeName As System.Text.StringBuilder, _
         ByRef size As Integer) As Boolean

    'フォアグランドウィンドウのハンドルを取得する
    Declare Function GetForegroundWindow Lib "user32" () As Integer


    Public intInterval As Integer = 60
    Dim strStatus As String = "監視中"
    Dim strAutLogfileName As String = ""
    Dim strHTMLLogfileName As String = ""
    Dim strFormerTask = ""
    Dim strTimeLine = ""   'タスクのタイムラインを保存する領域
    Dim strHTML = ""   'タスクのタイムラインを保存するHTML領域
    Public logDirectory As String = ""
#If DEBUG Then
    Dim intIntervalLineInsert As Integer = 1      '区切り線を挿入する間隔（分）
    Dim intNoOperationTimeout As Integer = 1       '無操作を検知する時間（分）
#Else
    Dim intIntervalLineInsert As Integer = 15      '区切り線を挿入する間隔（分）
    Dim intNoOperationTimeout As Integer = 5       '無操作を検知する時間（分）
#End If

    Dim intMousePosX = 0                            'マウスの座標保存用（x軸）
    Dim intMousePosY = 0                            'マウスの座標保存用（y軸）
    Dim dtLineInsertCounter As New DateTime     '区切り線を挿入する用のカウンター（DateTime）
    Dim dtNonOpeCounter As New DateTime     '無操作を検知するためのカウンタ（DateTime）
    Dim textBoxFocusON As Boolean = False
    Dim strTimeBgColor As String = "dark"  '時間列の背景色の指定
    Dim strWinTextBgColor As String = "dark"  'プロセス名、ウィンドウテキスト列の背景色の指定


    Private Sub Button_Start_Click(sender As Object, e As EventArgs) Handles Button_Start.Click
        strStatus = "監視中"
        Me.Text = "WinGetName(" + intInterval.ToString + "秒おきに" + strStatus + ")"
        NotifyIcon1.Text = "GetWinName(監視中)"

        ' タイマー開始（＝監視開始）
        Timer_Interval.Start()
    End Sub

    Private Sub Button_Stop_Click(sender As Object, e As EventArgs) Handles Button_Stop.Click
        strStatus = "監視停止"
        Me.Text = "WinGetName(監視停止)"
        NotifyIcon1.Text = "GetWinName(監視停止)"
        ' タイマー停止（＝監視停止）
        Timer_Interval.Stop()
    End Sub

    

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        '終了メッセージの追加
        strTimeLine = strTimeLine + Environment.NewLine + _
                DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 終了－－－－－－－－－－－－－－－－－－－－－－－－－"
        'HTMLに終了メッセージを追加

        'テキストログの保存
        SaveLogfileAuto()

        'HTMLログの保存
        strHTML = strHTML + "<tr id='last'><td class='color1_dark cell_nowrap time'></td>" +
                            "<td class='color2_dark cell_nowrap process'>終了</td>" +
                            "<td class='color2_dark cell_nowrap windowtext'></td></tr>" + Environment.NewLine
        'テンプレートファイルに挿入
        strHTML = My.Resources.cssframe_sample_log.Replace("<!-- WinNameInsertAREA -->", strHTML)
        'HTMLファイルに保存
        Dim sjisEnc As Encoding = Encoding.GetEncoding("Shift_JIS")
        Dim writer As StreamWriter
        Try
            writer = New StreamWriter(logDirectory + "\" + strHTMLLogfileName, _
                                       False, sjisEnc)

        Catch ex As System.IO.FileNotFoundException
            'FileNotFoundExceptionをキャッチした時
            Return
        Catch ex As System.IO.IOException
            'IOExceptionをキャッチした時
            Return
        Catch ex As System.UnauthorizedAccessException
            'UnauthorizedAccessExceptionをキャッチした時
            Return
        End Try

        writer.Write(strHTML)
        writer.Close()

    End Sub

    ' Formロード時に実行 各種初期化を行う
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load

        'XMLファイルから設定を読み込む
        '設定ファイルの存在有無をチェック
        Dim fileName As String = Settings.GetSettingPath
        If System.IO.File.Exists(fileName) Then
            '設定ファイルが既にある場合
            Settings.LoadFromXmlFile()
            intInterval = Settings.Instance.Interval
            logDirectory = Settings.Instance.LogPath
            SettingForm.CheckBox1.Checked = Settings.Instance.Baloon
            BaloonToolStripMenuItem.Checked = Settings.Instance.Baloon
        Else
            '設定ファイルがない場合はデフォルト設定
            intInterval = 60
            logDirectory = ""
            SettingForm.CheckBox1.Checked = True
            BaloonToolStripMenuItem.Checked = True

        End If



        ' ラベルとテキストボックスの初期化
        SettingForm.textBoxSettingInterval.Text = intInterval.ToString
        Me.Text = "WinGetName(" + intInterval.ToString + "秒おきに" + strStatus + ")"
        TextBox_Status.Text = ""
        strTimeLine = ""

        ' 監視間隔の初期設定、タイマーを開始
        Timer_Interval.Interval = intInterval * 1000
        Timer_Interval.Start()

        NotifyIcon1.Text = "GetWinName(監視中)"

        '区切り線挿入用カウンタ初期化
        Dim dtCounter As DateTime = DateTime.Now()    '一時的な計算用
        '無操作検知用カウンタ初期化
        dtNonOpeCounter = DateTime.Now()
        intMousePosX = System.Windows.Forms.Cursor.Position.X
        intMousePosY = System.Windows.Forms.Cursor.Position.Y

        '次の０／１５／３０／４５分を求める
        Dim intMinute As Integer = intIntervalLineInsert - _
            (dtCounter.Minute Mod intIntervalLineInsert)
        dtLineInsertCounter = dtCounter.AddMinutes(intMinute).AddSeconds(dtCounter.Second * (-1))

        '起動メッセージの表示
        strTimeLine = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " 起動－－－－－－－－－－－－－－－－－－－－－－－－－"
        TextBox_Status.Text = strTimeLine

        'HTMLログの保存
        strHTML = strHTML + "<tr id='top'><td class='color1_dark cell_nowrap time'>" +
                            DateTime.Now.ToString("HH:mm:ss") + "</td>" +
                            "<td class='color2_dark cell_nowrap process'>起動</td>" +
                            "<td class='color2_dark cell_nowrap windowtext'>" +
                            DateTime.Now.ToString("yyyy/MM/dd") + "</td></tr>" + Environment.NewLine

        'WebComponentの初期化
        Me.WebBrowser1.Width = 826 + 27
        Me.WebBrowser1.Navigate("about:blank")
        Me.WebBrowser1.Document.Write(String.Empty)
        Me.WebBrowser1.DocumentText = My.Resources.cssframe_sample

        '初回ログ保存
        SaveLogfileOnStart()

        'ログ保存
        SaveLogfileAuto()


    End Sub

    Private Sub Timer_Interval_Tick(sender As Object, e As EventArgs) Handles Timer_Interval.Tick
        Dim AWhwnd As Integer

        Dim lngResult As Integer
        Dim sb As New System.Text.StringBuilder("", 255)

        Dim sb2 As New System.Text.StringBuilder("", 255)

        Dim AWhwnd3 As Integer
        Dim lngResult3 As Integer
        Dim sb3 As New System.Text.StringBuilder("", 255)

        Dim hWnd As Integer
        Dim PID As Integer
        Dim hProcess As Integer

        Dim Modules(0 To 199) As Integer
        Dim mProcess As Integer
        Dim cbNeeded As Integer

        Dim sb4 As New System.Text.StringBuilder("", 255)
        Dim sb6 As New System.Text.StringBuilder("", 255)

        Dim strNoOperation As String = ""
        Dim strAnchor As String = ""

        'AWhwnd = GetActiveWindow  'ハンドルを取得
        AWhwnd = GetForegroundWindow  'ハンドルを取得
        'アクティブウィンドウのタイトルを取得
        lngResult = GetWindowText(AWhwnd, sb, 255)
        'ウィンドウハンドルからプロセスハンドルを取得
        GetWindowThreadProcessId(AWhwnd, PID)
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, PID)
        mProcess = EnumProcessModules(hProcess, Modules(0), 200, cbNeeded)
        GetModuleFileNameEx(hProcess, Modules(0), sb4, 255)
        GetModuleBaseName(hProcess, 0, sb6, 255)
        '
        QueryFullProcessImageName(hProcess, 0, sb2, 255)

        '区切り線を表示する（０／１５／３０／４５分になった場合）
        If DateTime.Now.CompareTo(dtLineInsertCounter) >= 0 Then   '現在時刻≧カウンタ時刻
            strTimeLine = strTimeLine + Environment.NewLine + _
                Date.Now.ToString("yyyy/MM/dd HH:mm") + "     －－－－－－－－－－－－－－－－－－－－－－－－－－－"

            '時間の背景色を変更
            If strTimeBgColor = "dark" Then
                strTimeBgColor = "light"
            Else
                strTimeBgColor = "dark"
            End If
            'anchor文字列を設定
            strAnchor = Date.Now.ToString("HHmm")

            '15分おきにログを自動保存
            SaveLogfileAuto()
            '次の０／１５／３０／４５分をカウンタにセット
            dtLineInsertCounter = dtLineInsertCounter.AddMinutes(intIntervalLineInsert)

            'バルーン表示がチェックされている場合はバルーンヒントに時刻を表示する。
            If SettingForm.CheckBox1.Checked = True Then
                'バルーンヒントに表示するメッセージ
                NotifyIcon1.BalloonTipText = Date.Now.ToString("HH:mm")
                '表示する時間をミリ秒で指定する
                NotifyIcon1.ShowBalloonTip(20000)
            End If

        End If

        '無操作を検知
        Dim tempMousePosX As Integer = System.Windows.Forms.Cursor.Position.X
        Dim tempMousePosY As Integer = System.Windows.Forms.Cursor.Position.Y

        If (tempMousePosX = intMousePosX) And (tempMousePosY = intMousePosY) Then
            If DateTime.Now.CompareTo(dtNonOpeCounter.AddMinutes(intNoOperationTimeout)) >= 0 Then
                '5分上経過していれば「操作なし　ｘ分経過」を出力
                AddRow(strTimeBgColor, "gray",
                       Date.Now.ToString("HH:mm:ss"),
                       "",
                       "操作なし " + Math.Floor((DateTime.Now - dtNonOpeCounter).TotalMinutes).ToString + "分経過",
                       strAnchor)
            Else
                '5分経っていなければ空白行を出力
                AddRow(strTimeBgColor, strWinTextBgColor, Date.Now.ToString("HH:mm:ss"), "", "", strAnchor)
            End If

        ElseIf (tempMousePosX = 0) And (tempMousePosY = 0) Then
            intMousePosX = 0
            intMousePosY = 0

            If DateTime.Now.CompareTo(dtNonOpeCounter.AddMinutes(intNoOperationTimeout)) >= 0 Then
                '5分上経過していれば「操作なし　ｘ分経過」を出力
                AddRow(strTimeBgColor, "gray",
                       Date.Now.ToString("HH:mm:ss"),
                       "",
                       "操作なし " + Math.Floor((DateTime.Now - dtNonOpeCounter).TotalMinutes).ToString + "分経過",
                       strAnchor)
            Else
                '5分経っていなければ空白行を出力
                AddRow(strTimeBgColor, strWinTextBgColor, Date.Now.ToString("HH:mm:ss"), "", "", strAnchor)
            End If

        Else
            '5分上経過していればログを出力
            'If DateTime.Now.CompareTo(dtNonOpeCounter.AddMinutes(intNoOperationTimeout)) >= 0 Then
            '    strTimeLine = strTimeLine + Environment.NewLine + _
            '    Date.Now.ToString("yyyy/MM/dd HH:mm:ss") + " : 操作なし " + _
            '    Math.Floor((DateTime.Now - dtNonOpeCounter).TotalMinutes).ToString(+"分経過")
            'End If

            'カウンタ再更新
            dtNonOpeCounter = DateTime.Now
            intMousePosX = tempMousePosX
            intMousePosY = tempMousePosY

            '結果を出力-
            Dim strTempTask = sb.ToString()
            If strFormerTask <> strTempTask Then
                strTimeLine = strTimeLine + Environment.NewLine + _
                  Date.Now.ToString("yyyy/MM/dd HH:mm:ss") + " : " + _
                  IO.Path.GetFileName(sb2.ToString()) + " : " + strTempTask
                strFormerTask = strTempTask

                'プロセス名、ウィンドウテキスト列の背景色を変更
                If strWinTextBgColor = "dark" Then
                    strWinTextBgColor = "light"
                Else
                    strWinTextBgColor = "dark"
                End If
                AddRow(strTimeBgColor, strWinTextBgColor, Date.Now.ToString("HH:mm:ss"),
                       IO.Path.GetFileName(sb2.ToString()), WebUtility.HtmlEncode(strTempTask), strAnchor)
            Else
                AddRow(strTimeBgColor, strWinTextBgColor, Date.Now.ToString("HH:mm:ss"), "", "", strAnchor)

            End If
        End If

        If Not textBoxFocusON Then
            TextBox_Status.Text = strTimeLine
        End If


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sjisEnc As Encoding = Encoding.GetEncoding("Shift_JIS")
        Dim writer As StreamWriter
        Try
            writer = New StreamWriter(logDirectory + "\wingetname" + DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".txt", _
                                       True, sjisEnc)

        Catch ex As System.IO.FileNotFoundException
            'FileNotFoundExceptionをキャッチした時
            MsgBox("ログの保存に失敗しました(FileNotFound)")
            Return
        Catch ex As System.IO.IOException
            'IOExceptionをキャッチした時
            MsgBox("ログの保存に失敗しました(IOException)")
            Return
        Catch ex As System.UnauthorizedAccessException
            'UnauthorizedAccessExceptionをキャッチした時
            MsgBox("ログの保存に失敗しました(UnauthorizedAccessException)")
            Return
        End Try
        writer.Write(strTimeLine)
        writer.Close()

    End Sub

    Private Sub AddRow(color1 As String, color2 As String, p1 As String, p2 As String, p3 As String, p4 As String)
        '引数の設定
        Dim args(5) As Object
        args = {color1, color2, p1, p2, p3, p4}

        'jacascriptの関数名を指定して呼び出し
        'addRow('dark','light','a','b','c','0900')"
        Me.WebBrowser1.Document.InvokeScript("addRow", args)

        'HTML用にも保存
        Dim tempTR As String = ""
        If p4 = "" Then
            tempTR = "<tr>"
        Else
            tempTR = "<tr id=" + p4 + ">"
        End If

        strHTML = strHTML + "<tr id=" + p4 + ">" +
                            "<td class='color1_" + color1 + " cell_nowrap time'><div class='cell_nowrap time'>" + p1 + "</div></td>" +
                            "<td class='color2_" + color2 + " cell_nowrap process'><div class='cell_nowrap process' title='" + p2 + "'>" + p2 + "</div></td>" +
                            "<td class='color2_" + color2 + " cell_nowrap windowtext'><div class='cell_nowrap windowtext' title='" + p3 + "'>" + p3 + "</div></td>" +
                            "</tr>" + Environment.NewLine

    End Sub

    Public Sub SaveLogfileOnStart()
        Dim sjisEnc As Encoding = Encoding.GetEncoding("Shift_JIS")
        Dim intCounter = 1

        Dim strLogfileName = "wingetname" + DateTime.Now.ToString("yyyy-MM-dd") + "_01"

        While System.IO.File.Exists(logDirectory + "\" + strLogfileName + ".txt")
            intCounter = intCounter + 1
            strLogfileName = "wingetname" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + intCounter.ToString("D2")
        End While

        strAutLogfileName = strLogfileName + ".txt"
        strHTMLLogfileName = strLogfileName + ".html"

    End Sub


    Private Sub SaveLogfileAuto()
        Dim sjisEnc As Encoding = Encoding.GetEncoding("Shift_JIS")
        Dim writer As StreamWriter
        Try
            writer = New StreamWriter(logDirectory + "\" + strAutLogfileName, _
                                       False, sjisEnc)

        Catch ex As System.IO.FileNotFoundException
            'FileNotFoundExceptionをキャッチした時
            Return
        Catch ex As System.IO.IOException
            'IOExceptionをキャッチした時
            Return
        Catch ex As System.UnauthorizedAccessException
            'UnauthorizedAccessExceptionをキャッチした時
            Return
        End Try

        writer.Write(strTimeLine)
        writer.Close()
    End Sub

    Private Sub 開くToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 開くToolStripMenuItem.Click
        ShowWindow()

    End Sub



    Private Sub 終了XToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles 終了XToolStripMenuItem.Click
        Me.Close()
    End Sub


    Private Sub NotifyIcon1_MouseClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseClick

        If e.Button = MouseButtons.Left Then
            ShowWindow()
        Else
            Me.ContextMenuStrip1.Show(Cursor.Position, ToolStripDropDownDirection.AboveLeft)
        End If

    End Sub

    Private Sub ShowWindow()
        Me.Visible = True
        Me.WindowState = FormWindowState.Normal

    End Sub

    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            'フォームを非表示にする
            Me.Visible = False
        End If

    End Sub

    Private Sub BaloonToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BaloonToolStripMenuItem.Click
        Dim item As ToolStripMenuItem = DirectCast(sender, ToolStripMenuItem)
        'チェック状態を反転させる
        item.Checked = Not item.Checked

        'チェックボックスにも反映
        Select Case item.Checked
            Case True
                SettingForm.CheckBox1.Checked = True
            Case False
                SettingForm.CheckBox1.Checked = False
        End Select
    End Sub



    Private Sub buttonOpenSetting_Click(sender As Object, e As EventArgs) Handles buttonOpenSetting.Click
        SettingForm.textBoxSettingInterval.Text = intInterval.ToString
        SettingForm.TextBoxFolderPath.Text = logDirectory
        SettingForm.textBoxSettingInterval.BackColor = SystemColors.Window
        SettingForm.TextBoxFolderPath.BackColor = SystemColors.Window

        SettingForm.ShowDialog()
    End Sub

    Private Sub TextBox_Status_Enter(sender As Object, e As EventArgs) Handles TextBox_Status.Enter
        TextBox_Status.BackColor = Color.LightGray
        btLock.Enabled = True
        textBoxFocusON = True
    End Sub

    Private Sub TextBox_Status_Leave(sender As Object, e As EventArgs) Handles TextBox_Status.Leave
        TextBox_Status.BackColor = Color.White
        TextBox_Status.Text = strTimeLine
        textBoxFocusON = False
        btLock.Enabled = False
    End Sub


    Private Sub KakoLog_Click(sender As Object, e As EventArgs) Handles KakoLog.Click
        KakoLogForm.ShowDialog()
    End Sub
End Class

'設定をXMLとして保存・読み込むためのクラス
' http://dobon.net/vb/dotnet/programing/storeappsettings.html
<Serializable()> _
Public Class Settings
    '設定を保存するフィールド
    Private _logPath As String
    Private _interval As Integer
    Private _baloon As Boolean

    '設定のプロパティ
    Public Property LogPath() As String
        Get
            Return _logPath
        End Get
        Set(ByVal Value As String)
            _logPath = Value
        End Set
    End Property

    Public Property Interval() As Integer
        Get
            Return _interval
        End Get
        Set(ByVal Value As Integer)
            _interval = value
        End Set
    End Property

    Public Property Baloon() As Integer
        Get
            Return _baloon
        End Get
        Set(ByVal Value As Integer)
            _baloon = Value
        End Set
    End Property

    'コンストラクタ
    Public Sub New()
        _logPath = ""
        _interval = 0
        _baloon = False
    End Sub

    'Settingsクラスのただ一つのインスタンス
    <NonSerialized()> _
    Private Shared _instance As Settings
    <System.Xml.Serialization.XmlIgnore()> _
    Public Shared Property Instance() As Settings
        Get
            If _instance Is Nothing Then
                _instance = New Settings
            End If
            Return _instance
        End Get
        Set(ByVal Value As Settings)
            _instance = Value
        End Set
    End Property

    ''' <summary>
    ''' 設定をXMLファイルから読み込み復元する
    ''' </summary>
    Public Shared Sub LoadFromXmlFile()
        Dim p As String = GetSettingPath()

        Dim sr As New StreamReader(p, New UTF8Encoding(False))
        Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(Settings))
        '読み込んで逆シリアル化する
        Dim obj As Object = xs.Deserialize(sr)
        sr.Close()

        Instance = CType(obj, Settings)
    End Sub

    ''' <summary>
    ''' 現在の設定をXMLファイルに保存する
    ''' </summary>
    Public Shared Sub SaveToXmlFile()
        Dim p As String = GetSettingPath()


        Dim sw As New StreamWriter(p, False, New UTF8Encoding(False))
        Dim xs As New System.Xml.Serialization.XmlSerializer(GetType(Settings))
        'シリアル化して書き込む
        xs.Serialize(sw, Instance)
        sw.Close()
    End Sub

    Public Shared Function GetSettingPath() As String
        Dim p As String = Application.ProductName + ".config"
        Return p
    End Function

    
End Class