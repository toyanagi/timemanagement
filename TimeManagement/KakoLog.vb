﻿Public Class KakoLogForm


    Private Sub KakoLog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'WebComponentの初期化
        Me.WebBrowser1.Width = 826 + 27
        Me.WebBrowser1.Navigate("about:blank")
        Me.WebBrowser1.Document.Write(String.Empty)
        Me.WebBrowser1.DocumentText = "<html><body><p align='center'>過去のログファイルを選択してください</p></body></html>"

        Debug.Print("log:" + Form1.logDirectory)
        Dim filesList As String()

        Try
            filesList = System.IO.Directory.GetFiles(Form1.logDirectory, "*.html", System.IO.SearchOption.TopDirectoryOnly)

        Catch ex As System.IO.FileNotFoundException
            'FileNotFoundExceptionをキャッチした時
            Return
        Catch ex As System.IO.IOException
            'IOExceptionをキャッチした時
            Return
        Catch ex As System.UnauthorizedAccessException
            'UnauthorizedAccessExceptionをキャッチした時
            Return
        End Try
        Debug.Print("log:" + filesList.Length.ToString)
        If filesList.Length = 0 Then
            Return
        End If

        Array.Sort(filesList)
        Array.Reverse(filesList)

        Dim maxList As Integer = filesList.Length
        If filesList.Length >= 20 Then
            maxList = 20
        End If

        ComboBox1.Items.Clear()

        For i As Integer = 0 To (maxList - 1)
            ComboBox1.Items.Add(System.IO.Path.GetFileName(filesList(i)))
        Next

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.WebBrowser1.Navigate(Form1.logDirectory + "\" + ComboBox1.SelectedValue)
        Debug.Print(Form1.logDirectory + "\" + ComboBox1.SelectedItem)
        '文字コード(ここでは、Shift JIS)
        Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("shift_jis")
        'テキストファイルの中身をすべて読み込む
        Dim str As String = System.IO.File.ReadAllText(Form1.logDirectory + "\" + ComboBox1.SelectedItem, enc)
        Me.WebBrowser1.Document.Write(String.Empty)
        Me.WebBrowser1.DocumentText = str
    End Sub
End Class