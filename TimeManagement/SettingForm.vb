﻿Public Class SettingForm


    Private Sub buttonSettingCancel_Click(sender As Object, e As EventArgs) Handles buttonSettingCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub buttonSettingSave_Click(sender As Object, e As EventArgs) Handles buttonSettingSave.Click

        '監視間隔を取得
        If IsNumeric(textBoxSettingInterval.Text) Then
            Settings.Instance.Interval = Integer.Parse(textBoxSettingInterval.Text)
            Form1.intInterval = Integer.Parse(textBoxSettingInterval.Text)
            Form1.Timer_Interval.Interval = Form1.intInterval * 1000
        Else
            textBoxSettingInterval.BackColor = Color.FromArgb(255, 255, 192)
            MsgBox("監視間隔には数値を指定して下さい。")
            Return
        End If


        'ログ保存先パスを取得
        If System.IO.Directory.Exists(TextBoxFolderPath.Text) Then
            Settings.Instance.LogPath = TextBoxFolderPath.Text
            Form1.logDirectory = TextBoxFolderPath.Text
        Else
            TextBoxFolderPath.BackColor = Color.FromArgb(255, 255, 192)
            MessageBox.Show("'" + TextBoxFolderPath.Text + "'は存在しません。")
            Return
        End If

        Settings.Instance.Baloon = CheckBox1.Checked
        Form1.BaloonToolStripMenuItem.Checked = CheckBox1.Checked

        '現在の設定をXMLファイルに保存する
        Settings.SaveToXmlFile()

        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub buttonSettingSetDir_Click(sender As Object, e As EventArgs) Handles buttonSettingSetDir.Click
        'FolderBrowserDialogクラスのインスタンスを作成
        Dim fbd As New FolderBrowserDialog

        '上部に表示する説明テキストを指定する
        fbd.Description = "フォルダを指定してください。"
        'ルートフォルダを指定する
        'デフォルトでDesktop
        fbd.RootFolder = Environment.SpecialFolder.Desktop
        '最初に選択するフォルダを指定する
        'RootFolder以下にあるフォルダである必要がある
        fbd.SelectedPath = "C:\Windows"
        'ユーザーが新しいフォルダを作成できるようにする
        'デフォルトでTrue
        fbd.ShowNewFolderButton = True

        'ダイアログを表示する
        If fbd.ShowDialog(Me) = DialogResult.OK Then
            '選択されたフォルダを表示する
            TextBoxFolderPath.Text = fbd.SelectedPath
        End If
    End Sub

    Private Sub SettingForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class