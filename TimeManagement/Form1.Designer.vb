﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Button_Start = New System.Windows.Forms.Button()
        Me.Button_Stop = New System.Windows.Forms.Button()
        Me.TextBox_Status = New System.Windows.Forms.TextBox()
        Me.Timer_Interval = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.開くToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BaloonToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.終了XToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileSystemWatcher1 = New System.IO.FileSystemWatcher()
        Me.buttonOpenSetting = New System.Windows.Forms.Button()
        Me.btLock = New System.Windows.Forms.Button()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.KakoLog = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button_Start
        '
        Me.Button_Start.Location = New System.Drawing.Point(12, 5)
        Me.Button_Start.Name = "Button_Start"
        Me.Button_Start.Size = New System.Drawing.Size(75, 23)
        Me.Button_Start.TabIndex = 1
        Me.Button_Start.Text = "監視開始"
        Me.Button_Start.UseVisualStyleBackColor = True
        '
        'Button_Stop
        '
        Me.Button_Stop.Location = New System.Drawing.Point(93, 5)
        Me.Button_Stop.Name = "Button_Stop"
        Me.Button_Stop.Size = New System.Drawing.Size(75, 23)
        Me.Button_Stop.TabIndex = 2
        Me.Button_Stop.Text = "監視停止"
        Me.Button_Stop.UseVisualStyleBackColor = True
        '
        'TextBox_Status
        '
        Me.TextBox_Status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox_Status.Location = New System.Drawing.Point(12, 34)
        Me.TextBox_Status.Margin = New System.Windows.Forms.Padding(0)
        Me.TextBox_Status.Multiline = True
        Me.TextBox_Status.Name = "TextBox_Status"
        Me.TextBox_Status.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox_Status.Size = New System.Drawing.Size(853, 53)
        Me.TextBox_Status.TabIndex = 6
        Me.TextBox_Status.TabStop = False
        Me.TextBox_Status.Visible = False
        '
        'Timer_Interval
        '
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.Button1.Location = New System.Drawing.Point(174, 5)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "ログ出力"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "GetWinName"
        Me.NotifyIcon1.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.開くToolStripMenuItem, Me.BaloonToolStripMenuItem, Me.ToolStripMenuItem1, Me.終了XToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(149, 76)
        '
        '開くToolStripMenuItem
        '
        Me.開くToolStripMenuItem.Name = "開くToolStripMenuItem"
        Me.開くToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.開くToolStripMenuItem.Text = "開く(&O)"
        '
        'BaloonToolStripMenuItem
        '
        Me.BaloonToolStripMenuItem.Checked = True
        Me.BaloonToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.BaloonToolStripMenuItem.Name = "BaloonToolStripMenuItem"
        Me.BaloonToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.BaloonToolStripMenuItem.Text = "バルーン表示"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(145, 6)
        '
        '終了XToolStripMenuItem
        '
        Me.終了XToolStripMenuItem.Name = "終了XToolStripMenuItem"
        Me.終了XToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.終了XToolStripMenuItem.Text = "終了(X)"
        '
        'FileSystemWatcher1
        '
        Me.FileSystemWatcher1.EnableRaisingEvents = True
        Me.FileSystemWatcher1.SynchronizingObject = Me
        '
        'buttonOpenSetting
        '
        Me.buttonOpenSetting.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.buttonOpenSetting.Location = New System.Drawing.Point(773, 5)
        Me.buttonOpenSetting.Name = "buttonOpenSetting"
        Me.buttonOpenSetting.Size = New System.Drawing.Size(92, 23)
        Me.buttonOpenSetting.TabIndex = 6
        Me.buttonOpenSetting.Text = "設定を開く"
        Me.buttonOpenSetting.UseVisualStyleBackColor = True
        '
        'btLock
        '
        Me.btLock.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.btLock.Enabled = False
        Me.btLock.Location = New System.Drawing.Point(255, 5)
        Me.btLock.Name = "btLock"
        Me.btLock.Size = New System.Drawing.Size(75, 23)
        Me.btLock.TabIndex = 4
        Me.btLock.Text = "ロック解除"
        Me.btLock.UseVisualStyleBackColor = True
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.WebBrowser1.IsWebBrowserContextMenuEnabled = False
        Me.WebBrowser1.Location = New System.Drawing.Point(12, 34)
        Me.WebBrowser1.Margin = New System.Windows.Forms.Padding(0)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.ScriptErrorsSuppressed = True
        Me.WebBrowser1.Size = New System.Drawing.Size(853, 568)
        Me.WebBrowser1.TabIndex = 7
        '
        'KakoLog
        '
        Me.KakoLog.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.KakoLog.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.KakoLog.Location = New System.Drawing.Point(692, 5)
        Me.KakoLog.Name = "KakoLog"
        Me.KakoLog.Size = New System.Drawing.Size(75, 23)
        Me.KakoLog.TabIndex = 5
        Me.KakoLog.Text = "過去ログ"
        Me.KakoLog.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(867, 614)
        Me.Controls.Add(Me.KakoLog)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.btLock)
        Me.Controls.Add(Me.buttonOpenSetting)
        Me.Controls.Add(Me.TextBox_Status)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button_Stop)
        Me.Controls.Add(Me.Button_Start)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(883, 1000)
        Me.MinimumSize = New System.Drawing.Size(883, 38)
        Me.Name = "Form1"
        Me.Text = "GetWinName"
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_Start As System.Windows.Forms.Button
    Friend WithEvents Button_Stop As System.Windows.Forms.Button
    Friend WithEvents TextBox_Status As System.Windows.Forms.TextBox
    Friend WithEvents Timer_Interval As System.Windows.Forms.Timer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents 開くToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 終了XToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BaloonToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileSystemWatcher1 As System.IO.FileSystemWatcher
    Friend WithEvents buttonOpenSetting As System.Windows.Forms.Button
    Friend WithEvents btLock As System.Windows.Forms.Button
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents KakoLog As System.Windows.Forms.Button

End Class
