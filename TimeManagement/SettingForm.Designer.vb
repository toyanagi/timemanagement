﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SettingForm
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SettingForm))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.textBoxSettingInterval = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxFolderPath = New System.Windows.Forms.TextBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.buttonSettingSave = New System.Windows.Forms.Button()
        Me.buttonSettingCancel = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.buttonSettingSetDir = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 12)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "監視間隔(秒)"
        '
        'textBoxSettingInterval
        '
        Me.textBoxSettingInterval.Location = New System.Drawing.Point(111, 6)
        Me.textBoxSettingInterval.Name = "textBoxSettingInterval"
        Me.textBoxSettingInterval.Size = New System.Drawing.Size(81, 19)
        Me.textBoxSettingInterval.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 12)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "ログファイル保存先"
        '
        'TextBoxFolderPath
        '
        Me.TextBoxFolderPath.Location = New System.Drawing.Point(111, 35)
        Me.TextBoxFolderPath.Name = "TextBoxFolderPath"
        Me.TextBoxFolderPath.Size = New System.Drawing.Size(326, 19)
        Me.TextBoxFolderPath.TabIndex = 2
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Location = New System.Drawing.Point(14, 67)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(87, 16)
        Me.CheckBox1.TabIndex = 4
        Me.CheckBox1.Text = "バルーン表示"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'buttonSettingSave
        '
        Me.buttonSettingSave.Location = New System.Drawing.Point(443, 86)
        Me.buttonSettingSave.Name = "buttonSettingSave"
        Me.buttonSettingSave.Size = New System.Drawing.Size(91, 23)
        Me.buttonSettingSave.TabIndex = 6
        Me.buttonSettingSave.Text = "設定を保存"
        Me.buttonSettingSave.UseVisualStyleBackColor = True
        '
        'buttonSettingCancel
        '
        Me.buttonSettingCancel.Location = New System.Drawing.Point(346, 86)
        Me.buttonSettingCancel.Name = "buttonSettingCancel"
        Me.buttonSettingCancel.Size = New System.Drawing.Size(91, 23)
        Me.buttonSettingCancel.TabIndex = 5
        Me.buttonSettingCancel.Text = "キャンセル"
        Me.buttonSettingCancel.UseVisualStyleBackColor = True
        '
        'buttonSettingSetDir
        '
        Me.buttonSettingSetDir.Location = New System.Drawing.Point(443, 35)
        Me.buttonSettingSetDir.Name = "buttonSettingSetDir"
        Me.buttonSettingSetDir.Size = New System.Drawing.Size(91, 23)
        Me.buttonSettingSetDir.TabIndex = 3
        Me.buttonSettingSetDir.Text = "フォルダを選択"
        Me.buttonSettingSetDir.UseVisualStyleBackColor = True
        '
        'SettingForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(540, 116)
        Me.Controls.Add(Me.buttonSettingSetDir)
        Me.Controls.Add(Me.buttonSettingCancel)
        Me.Controls.Add(Me.buttonSettingSave)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBoxFolderPath)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.textBoxSettingInterval)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "SettingForm"
        Me.Text = "WinGetName 設定"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents textBoxSettingInterval As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxFolderPath As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents buttonSettingSave As System.Windows.Forms.Button
    Friend WithEvents buttonSettingCancel As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents buttonSettingSetDir As System.Windows.Forms.Button
End Class
